import sys, os
root_folder, _ = os.path.split(os.path.abspath(os.path.dirname(__file__) ))
sys.path.append(root_folder)

from processors.data_populator import DataPopulator
from processors.restaurant_locator import RestaurantLocator

def populate_data(csv_file_name):
	print 'preprocessing...'
	data_populator = DataPopulator()
	master_data = data_populator.populate_data_with_csv(csv_file_name)
	return master_data

def find_min_price(master_data, food_name_list):
	restaurant_locator = RestaurantLocator(master_data)
	print 'getting list ...'
	return restaurant_locator.find_min_price(food_name_list)

def get_input_params(args):
	try:
		input_args = args[:]
		data_file_name = input_args.pop(0)
		_, file_extension = os.path.os.path.splitext(data_file_name)
		if file_extension != '.csv':
			#file extension validator
			print 'invalid data file format'
			return None,  None

		
		if not input_args:
			print 'please supply customer choice of food'
			return None,  None
		else:
			food_name_list = [food_name.strip() for food_name in input_args]
			return data_file_name, food_name_list

	except IndexError, e:
		print 'insufficient arguments'
		return None,  None


def search_restaurant(args):
	data_file_name, food_name_list = get_input_params(args)
	restaurant_id, total_min_price = None, None

	if data_file_name and food_name_list:
		csv_file_name = os.path.join(root_folder, 'resources', data_file_name)
		master_data = populate_data(csv_file_name)
		restaurant_id, total_min_price = find_min_price(master_data, food_name_list)

	return restaurant_id, total_min_price

if __name__== '__main__':
	input_args = sys.argv[1:]
	restaurant_id, total_min_price = search_restaurant(input_args)
	print restaurant_id, total_min_price


