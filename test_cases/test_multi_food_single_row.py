__author__ = 'ankit'

from test_cases.test_base import UnitTestBase

class UnitSampleDataTest(UnitTestBase):
    
    def testWrongExtension(self):
        arg_list = ['multi_food_single_row.txt', 'fancy_european_water']
        result = self.search_restaurant_in_test(arg_list)
        restaurant_id, total_min_price = result[0], result[1]
        assert(restaurant_id is None and total_min_price is None)
        
    def testMultiItem(self):
        arg_list = ['multi_food_single_row.csv', 'jalapeno_poppers', 'extreme_fajita']
        result = self.search_restaurant_in_test(arg_list)
        restaurant_id, total_min_price = result[0], result[1]
        print restaurant_id, total_min_price, 'here'
        assert(restaurant_id=='6' and total_min_price==6)
        
    def testSingleItem1(self):
        arg_list = ['multi_food_single_row.csv', 'jalapeno_poppers']
        result = self.search_restaurant_in_test(arg_list)
        restaurant_id, total_min_price = result[0], result[1]
        print restaurant_id, total_min_price, 'here'
        assert(restaurant_id=='1' and total_min_price==4)
            
    def testNoItem(self):
        arg_list = ['multi_food_single_row.csv']
        result = self.search_restaurant_in_test(arg_list)
        restaurant_id, total_min_price = result[0], result[1]
        assert(restaurant_id is None and total_min_price is None)
        
    def testSingleItem(self):
        arg_list = ['multi_food_single_row.csv', 'fancy_european_water' ]
        result = self.search_restaurant_in_test(arg_list)
        restaurant_id, total_min_price = result[0], result[1]
        assert(restaurant_id=='6' and total_min_price==5.0)

    
