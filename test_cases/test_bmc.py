__author__ = 'ankit'

from test_cases.test_base import UnitTestBase

class UnitBMCTest(UnitTestBase):
    
    def testWrongExtension(self):
        arg_list = ['burger_momo_chowmien.csv.txt', 'fancy_european_water']
        result = self.search_restaurant_in_test(arg_list)
        restaurant_id, total_min_price = result[0], result[1]
        assert(restaurant_id is None and total_min_price is None)
        
    def testMultiItem(self):
        arg_list = ['burger_momo_chowmien.csv', 'burger', 'momo', 'chowmien', 'pepsi']
        result = self.search_restaurant_in_test(arg_list)
        restaurant_id, total_min_price = result[0], result[1]
        print restaurant_id, total_min_price, 'here'
        assert(restaurant_id=='1' and total_min_price==8)
        
    def testMultiItem1(self):
        arg_list = ['burger_momo_chowmien.csv', 'burger', 'momo']
        result = self.search_restaurant_in_test(arg_list)
        restaurant_id, total_min_price = result[0], result[1]
        print restaurant_id, total_min_price, 'here'
        assert(restaurant_id=='1' and total_min_price==5)
        
    def testMultiItem2(self):
        arg_list = ['burger_momo_chowmien.csv', 'burger', 'momo', 'chowmien']
        result = self.search_restaurant_in_test(arg_list)
        restaurant_id, total_min_price = result[0], result[1]
        print restaurant_id, total_min_price, 'here'
        assert(restaurant_id=='1' and total_min_price==8)
            
#    def testNoItem(self):
#        arg_list = ['burger_momo_chowmien.csv']
#        result = self.search_restaurant_in_test(arg_list)
#        restaurant_id, total_min_price = result[0], result[1]
#        assert(restaurant_id is None and total_min_price is None)
#        
#    def testSingleItem(self):
#        arg_list = ['burger_momo_chowmien.csv', 'fancy_european_water' ]
#        result = self.search_restaurant_in_test(arg_list)
#        restaurant_id, total_min_price = result[0], result[1]
#        assert(restaurant_id=='6' and total_min_price==5.0)
#
#    
