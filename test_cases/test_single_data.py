'''
Created on 30-Dec-2013

@author: ankit
'''

from test_cases.test_base import UnitTestBase

class UnitSingleDataTest(UnitTestBase):
    def testWrongExtension(self):
        arg_list = ['single_item_data.txt', 'fancy_european_water']
        result = self.search_restaurant_in_test(arg_list)
        restaurant_id, total_min_price = result[0], result[1]
        assert(restaurant_id is None and total_min_price is None)
        
        
    def testNoItem(self):
        arg_list = ['single_item_data.csv']
        result = self.search_restaurant_in_test(arg_list)
        restaurant_id, total_min_price = result[0], result[1]
        assert(restaurant_id is None and total_min_price is None)
        
    def testSingleItem(self):
        arg_list = ['single_item_data.csv', 'fancy_european_water' ]
        result = self.search_restaurant_in_test(arg_list)
        restaurant_id, total_min_price = result[0], result[1]
        assert(restaurant_id=='6' and total_min_price==5.0)

    def testMultiItem(self):
        arg_list = ['single_item_data.csv', 'fancy_european_water', 'extreme_fajita']
        result = self.search_restaurant_in_test(arg_list)
        restaurant_id, total_min_price = result[0], result[1]
        assert(restaurant_id is None and total_min_price is None)
