__author__ = 'ankit'

from test_cases.test_base import UnitTestBase

class UnitSampleDataTest(UnitTestBase):
    
    def testWrongExtension(self):
        arg_list = ['sample_data.txt', 'fancy_european_water']
        result = self.search_restaurant_in_test(arg_list)
        restaurant_id, total_min_price = result[0], result[1]
        assert(restaurant_id is None and total_min_price is None)
        
        
    def testNoItem(self):
        arg_list = ['sample_data.csv']
        result = self.search_restaurant_in_test(arg_list)
        restaurant_id, total_min_price = result[0], result[1]
        assert(restaurant_id is None and total_min_price is None)
        
    def testSingleItem(self):
        arg_list = ['sample_data.csv', 'fancy_european_water' ]
        result = self.search_restaurant_in_test(arg_list)
        restaurant_id, total_min_price = result[0], result[1]
        assert(restaurant_id=='6' and total_min_price==5.0)

    def testMultiItem(self):
        arg_list = ['sample_data.csv', 'fancy_european_water', 'extreme_fajita']
        result = self.search_restaurant_in_test(arg_list)
        restaurant_id, total_min_price = result[0], result[1]
        assert(restaurant_id=='6' and total_min_price==11.0)
        
    def testExceptionCase(self):
        arg_list = ['sample_data.csv', 'burger', 'tofu_log', 'chef']
        result = self.search_restaurant_in_test(arg_list)
        restaurant_id, total_min_price = result[0], result[1]
        assert(restaurant_id==None and total_min_price==None)
        
