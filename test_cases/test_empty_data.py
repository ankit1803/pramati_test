'''
Created on 30-Dec-2013

@author: ankit
'''
from test_cases.test_base import UnitTestBase

class UnitEmptyDataTest(UnitTestBase):
    def testWrongExtension(self):
        arg_list = ['empty_data.txt', 'fancy_european_water']
        result = self.search_restaurant_in_test(arg_list)
        restaurant_id, total_min_price = result[0], result[1]
        assert(restaurant_id is None and total_min_price is None)
        
        
    def testNoItem(self):
        arg_list = ['empty_data.csv']
        result = self.search_restaurant_in_test(arg_list)
        restaurant_id, total_min_price = result[0], result[1]
        assert(restaurant_id is None and total_min_price is None)
        
    def testSingleItem(self):
        arg_list = ['empty_data.csv', 'fancy_european_water' ]
        result = self.search_restaurant_in_test(arg_list)
        restaurant_id, total_min_price = result[0], result[1]
        assert(restaurant_id is None and total_min_price is None)

    def testMultiItem(self):
        arg_list = ['empty_data.csv', 'fancy_european_water', 'extreme_fajita']
        result = self.search_restaurant_in_test(arg_list)
        restaurant_id, total_min_price = result[0], result[1]
        assert(restaurant_id is None and total_min_price is None)
