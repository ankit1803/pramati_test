﻿How to :
1. Program python file is app/locate_restaurant.py
2. Command to run, if in root folder, will be : 
python app/locate_restaurant.py DATA_FILE [FOOD ITEM1, FOODITEM2, ...]
3. Command take 2 mandatory arguements, 
DATA_FILE: a csv format data file , inside the resource folder , with 3 columns, RESTAURANT_ID, FOOD_PRICE, FOOD_NAME
FOOD_ITEM_LIST: list of space separated food items that a customer wants to eat .
4. The command prints resturant_id, price , where price is the minimum for the given list of food items.
