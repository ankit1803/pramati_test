'''
Created on 04-Jan-2014

@author: ankit
'''

class ComboItem(object):

    def __init__(self, food_name_list, price):
        self.food_item_list = food_name_list
        self.price = price
        
    def __str__(self):
        return "{0} {1}".format(str(self.food_item_list), str(self.price))
    
    def __repr__(self):
        return "{0} {1}".format(str(self.food_item_list), str(self.price))
        