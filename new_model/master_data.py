'''
Created on 03-Jan-2014

@author: ankit
'''
from new_model.restaurant import Restaurant

class MasterData(object):

    def __init__(self):
        self._rid_restaurant_map = {}
        self.food_restaurant_map = {}
    
    def get_restaurant_by_id(self, rid):
        try:
            restuarant = self._rid_restaurant_map[rid]
        except KeyError, e:
            restuarant = Restaurant(rid)
            self._rid_restaurant_map[rid] = restuarant
        return restuarant
    
    def _add_to_food_restaurant_map(self, rid, food_name_list):
        for food_name in food_name_list:
            try:
                restaurant_set = self.food_restaurant_map[food_name]
            except KeyError, e:
                restaurant_set = set()
            restaurant_set.add(rid)
            self.food_restaurant_map[food_name] = restaurant_set
    
    def add_restuarant_food_item(self, rid, food_name_list, price):
        restuarant = self.get_restaurant_by_id(rid)
        restuarant.add_food_item(food_name_list, price)
        
        self._add_to_food_restaurant_map(rid, food_name_list)
        