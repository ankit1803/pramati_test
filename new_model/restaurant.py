'''
Created on 03-Jan-2014

@author: ankit
'''
from new_model.combo_item import ComboItem
import copy

class Restaurant(object):

    def __init__(self, rid):
        self.rid = rid
        self._combo_item_map = {}
        
    def __str__(self):
        return self.rid
    
    def __repr__(self):
        return self.rid
        
    def add_food_item(self, food_name_list, price):
        combo_item = ComboItem(food_name_list, price)
        for food_name in food_name_list:
            try:
                food_combo_list = self._combo_item_map[food_name]
            except KeyError, _:
                food_combo_list = []
            food_combo_list = self._sorted_insert(combo_item, 
                                                    food_combo_list)
             
            self._combo_item_map[food_name] = food_combo_list
            
    def _sorted_insert(self, combo_item, food_combo_list): 
        index = 0
        combo_list_copy = food_combo_list[:]
        for food_combo in combo_list_copy:
            if combo_item.price < food_combo.price:
                combo_list_copy.insert(index, combo_item)
                return combo_list_copy
            index += 1
        combo_list_copy.append(combo_item)
        return combo_list_copy
            
    def get_min_total_price(self, order_list):
        min_total_price = 0
        filled_items = {}
        all_items_added = set()
        
        order_list_copy = order_list[:]
        order_set = set(order_list)
                
        for food_name in order_list:
            '''
            for each newly added item from the order_list,
            search again for the changed minimum price
            '''
            combo_item, filled_items, min_total_price = self._search_minimum_total(food_name, min_total_price, 
                                                    filled_items, order_list_copy, all_items_added)             
#            for new_food_name in new_food_items:
            if combo_item:
                all_items_added = order_set.intersection(set(combo_item.food_item_list))
        return min_total_price, filled_items
    
            
    def _search_minimum_total(self, food_name, current_total_price, 
                              filled_items, order_list, all_items_added):
        '''
        take a current state which have current total price and filled items and a new item
        which needs to be added. 
        iterate through the mapped list of combos for the new item and check for the minimum price
        '''
        combo_list = self._combo_item_map[food_name]
        combo_result = None
        min_total_price = current_total_price
             
        updated_filled_items = {}                   
        if filled_items:
            filled_items_copy = copy.deepcopy(filled_items)
            for combo_item in combo_list: 
                new_filled_items = {}                   
                coll_removed_items, cost_cut = self._get_collaterally_removed_items(combo_item, food_name,
                                                                                       filled_items_copy,
                                                                                       order_list)
                min_price_coll_items = 0
                if coll_removed_items:
                    min_price_coll_items, new_filled_items = self.get_min_total_price(coll_removed_items)
                new_price = current_total_price-cost_cut + ( combo_item.price+min_price_coll_items )
                
                if new_price < current_total_price + combo_list[0].price:
                    combo_result = combo_item
                    min_total_price = new_price
                    updated_filled_items.update(filled_items_copy)
                    updated_filled_items.update(new_filled_items)
        
        if combo_result is None:
            if food_name not in all_items_added:
                combo_result = combo_list[0]
                min_total_price = current_total_price + combo_list[0].price
                filled_items[food_name] = combo_result
        else:
            filled_items = updated_filled_items
            filled_items[food_name] = combo_result                                       
                    
        return combo_result, filled_items, min_total_price
    
    def _get_collaterally_removed_items(self, combo_item, food_name, 
                                        filled_items, order_list):
        '''
        The idea is if a we have a new combo which have items [a1, a3]
        where a1 is already mapped with its combo [a1, a2] then in that case
        we remove a1 as it is already present in new combo , but by removing a1
        we also remove a2 , so now we marked a2 as collaterely and send it again
        to find a2's price indifferently .
        '''
        cost_cut = 0        
        viable_to_remove_set = set(combo_item.food_item_list)-set([food_name])
        
        
        filled_items_copy = copy.deepcopy(filled_items)
        removed_items = set()
        for viable_food_name in viable_to_remove_set:
            for existing_name, existing_combo in filled_items_copy.items():
                if viable_food_name in existing_combo.food_item_list:
                    for items in existing_combo.food_item_list:
                        removed_items.add(items)
                    try:
                        filled_items.pop(existing_name)
                        cost_cut += existing_combo.price
                    except KeyError, e:
                        continue
            filled_items_copy = copy.deepcopy(filled_items)
            
        removed_items = removed_items.intersection(set(order_list)) - set(combo_item.food_item_list)
        
        return list(removed_items), cost_cut