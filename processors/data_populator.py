'''
Created on 05-Jan-2014

@author: ankit
'''
from new_model.master_data import MasterData
import csv

RESTUARANT_ID = 0
FOOD_PRICE = 1
FOOD_NAME = 2


class DataPopulator(object):
    
    def __init__(self):
        pass
    
    def populate_data_with_csv(self, csv_file):
        master_data = MasterData()

        data_reader = csv.reader(open(csv_file, 'r'))
        index = 1
        for row in data_reader:
            try:
                restuarant_id = row[RESTUARANT_ID]
                food_price = float(row[FOOD_PRICE].strip())
                food_name_list = []
                for food_name in row[FOOD_NAME:]:
                    if food_name:
                        food_name_list.append(food_name.strip())
                master_data.add_restuarant_food_item(restuarant_id.strip(), food_name_list, food_price)
                index += 1
            except Exception, e:
                print 'Error: {0} ,found in row {1}'.format(e, index)

        return master_data