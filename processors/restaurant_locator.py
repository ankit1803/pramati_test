'''
Created on 05-Jan-2014

@author: ankit
'''

class RestaurantLocator(object):
    
    def __init__(self, master_data):
        self.master_data = master_data
        pass
    
    def _restaurant_list_all_items_available(self, food_item_name_list):
        main_set = None
        for food_item_name in food_item_name_list:
            try:
                restaurant_set = self.master_data.food_restaurant_map[food_item_name]
                if main_set == None:
                    #hack for first iteration
                    main_set = restaurant_set

                main_set = restaurant_set.intersection(main_set)
            except KeyError:
                return None
        return main_set

    def find_min_price(self, food_name_list):
        '''
        it returns the restaurant id, minimum total price and if no match found
        it returns None, None.
        it finds a list of restaurant id which have all the food available in the 
        food_name_list .
        Iterate through all the restuarant and get their minimum total price.
        Stores the global minimum price and restaurant id and return it as a result
        '''
        
        effective_restaurant_ids = self._restaurant_list_all_items_available(food_name_list)
        if effective_restaurant_ids is None:
            effective_restaurant_ids = []  
        total_min_price = None
        result_restaurant_id = None
    
        for restaurant_id in effective_restaurant_ids:
            '''
            iterate thorugh a list of restuarant where all the food items are available in each,
            find price for each and return the minimum price
            '''
            restaurant = self.master_data.get_restaurant_by_id(restaurant_id)
    
            restauarant_total_price, combos_selected = restaurant.get_min_total_price(food_name_list)
            if total_min_price == None:
                #a hack to calculate min price for first iteration
                total_min_price = restauarant_total_price
                result_restaurant_id = restaurant.rid
            elif total_min_price > restauarant_total_price:
                total_min_price = restauarant_total_price
                result_restaurant_id = restaurant.rid
        return result_restaurant_id, total_min_price
    
    